﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics.CodeAnalysis;
using Vostok.Logging.Abstractions;

namespace MyWebApi.Core.Logging
{
    public static class Log
    {
        private static readonly ConcurrentDictionary<string, ILog> logs = new();
        public static ILog DefaultLogger => LogProvider.Get();

        public static ILog For<T>()
        {
            return For(typeof(T));
        }

        public static ILog For<T>(T instance)
        {
            return For<T>();
        }

        public static ILog For([NotNull] Type type)
        {
            return GetLoggerForContext(type.Name);
        }

        public static ILog For([NotNull] string contextName)
        {
            return GetLoggerForContext(contextName);
        }

        private static ILog GetLoggerForContext([NotNull] string contextName)
        {
            return logs.GetOrAdd(contextName, t => DefaultLogger.ForContext(contextName));
        }
    }
}