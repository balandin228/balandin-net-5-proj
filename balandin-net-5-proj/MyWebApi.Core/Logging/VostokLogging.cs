﻿using System;
using System.IO;
using Vostok.Logging.Abstractions;
using Vostok.Logging.Console;
using Vostok.Logging.File;
using Vostok.Logging.File.Configuration;

namespace MyWebApi.Core.Logging
{
    public static class VostokLogging
    {
        private static ILog? _logInstance;

        public static ILog Logger
        {
            get { return _logInstance ??= CreateLogger(); }
        }

        private static ILog CreateLogger()
        {
            var fileLog = new FileLog(new FileLogSettings
            {
                FilePath = Path.Combine("logs", $"log.{{RollingSuffix}}.{DateTime.Now:HH-mm-ss}.log"),
                RollingStrategy = new RollingStrategyOptions
                {
                    Type = RollingStrategyType.ByTime,
                    Period = RollingPeriod.Day,
                    MaxFiles = 10
                },
                FileOpenMode = FileOpenMode.Append
            });

            var consoleLog = new ConsoleLog();


            return new CompositeLog(fileLog, consoleLog);
        }
    }
}