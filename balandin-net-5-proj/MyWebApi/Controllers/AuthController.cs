﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MyWebApi.Authentication;
using MyWebApi.DataAccess.Users;
using MyWebApi.Models.Authentication;

namespace MyWebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly AuthOptions _authOptions;
        private readonly IJwtService _jwtService;
        private readonly UserManager<ApplicationUser> _userManager;

        public AuthController(IJwtService jwtService, UserManager<ApplicationUser> userManager,
            IOptions<AuthOptions> authOptions)
        {
            _jwtService = jwtService;
            _userManager = userManager;
            _authOptions = authOptions.Value;
        }

        /// <summary>
        ///     login
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("login")]
        [HttpPost]
        [ProducesResponseType(typeof(LoginResponse), StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<LoginResponse>> Login([FromBody] LoginModel loginModel)
        {
            if (string.IsNullOrEmpty(loginModel.Login) || string.IsNullOrEmpty(loginModel.Password))
                return BadRequest();

            var user = await _userManager.FindByEmailAsync(loginModel.Login);
            if (user == null)
                return NotFound("Пользователь не найден");
            var result = await _userManager.CheckPasswordAsync(user, loginModel.Password);
            if (!result)
                return BadRequest("Неверное имя пользователя или пароль");
            return Ok(new LoginResponse
            {
                AccessToken = _jwtService.BuildToken(loginModel.Login, "user"), ExpiresIn = _authOptions.TokenLifetime
            });
        }

        /// <summary>
        ///     register
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns></returns>
        /// <exception cref="AggregateException"></exception>
        [AllowAnonymous]
        [Route("register")]
        [HttpPost]
        public async Task<IActionResult> Register([FromBody] LoginModel loginModel)
        {
            var user = new ApplicationUser
            {
                UserName = loginModel.Login,
                Email = loginModel.Login
            };
            var result = await _userManager.CreateAsync(user);
            if (!result.Succeeded)
                throw new AggregateException("Oops");

            await _userManager.AddPasswordAsync(user, loginModel.Password);

            return Ok();
        }
    }
}