﻿namespace MyWebApi.Authentication
{
    public interface IJwtService
    {
        string BuildToken(string userName, string password);

        //string GenerateJSONWebToken(string key, string issuer, UserDTO user);
        bool IsTokenValid(string key, string issuer, string token);
    }
}