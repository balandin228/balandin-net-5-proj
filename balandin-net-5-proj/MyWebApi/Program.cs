using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using MyWebApi.Core.Logging;
using Vostok.Logging.Microsoft;

namespace MyWebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureLogging(x => x.AddVostok(VostokLogging.Logger))
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
        }
    }
}