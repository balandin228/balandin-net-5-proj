﻿namespace MyWebApi
{
    public static class WebDefaults
    {
        public const string ApiTitle = "My Web-api template";
        public const string ApiDocName = "v1_my_webApi";
        public const string AuthSchemeName = "JWT_Token";
        public const string AuthHeaderName = "Authorization";
    }
}