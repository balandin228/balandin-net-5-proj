﻿using System.ComponentModel.DataAnnotations;

namespace MyWebApi.Models.Authentication
{
    public class LoginModel
    {
        [Required] public string Login { get; set; }

        [Required] public string Password { get; set; }
    }
}