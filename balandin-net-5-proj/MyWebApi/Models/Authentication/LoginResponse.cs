﻿namespace MyWebApi.Models.Authentication
{
    public class LoginResponse
    {
        /// <summary>
        ///     Jwt access token
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        ///     Expire time in minutes
        /// </summary>
        public int ExpiresIn { get; set; }
    }
}