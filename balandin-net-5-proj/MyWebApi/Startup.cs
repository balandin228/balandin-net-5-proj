using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using MyWebApi.Authentication;
using MyWebApi.Core.Logging;
using MyWebApi.DataAccess;
using MyWebApi.Extensions;
using Vostok.Logging.Abstractions;

namespace MyWebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvcCore()
                .AddApiExplorer();

            var authOptionsConfiguration = Configuration.GetSection("Auth");
            services.Configure<AuthOptions>(authOptionsConfiguration);
            var authOptions = authOptionsConfiguration.Get<AuthOptions>();

            services.AddAuthorization();

            services.AddIdentity();

            services.AddAuthentication(auth =>
                {
                    auth.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    auth.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    auth.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = authOptions.Issuer,
                        ValidAudience = authOptions.Audience,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(authOptions.Secret))
                    };
                });

            Configuration.GetConnectionString(ConnectionStrings.MyApiDbContext);
            services.AddDbContext<MyApiDbContext>(opt =>
                opt.UseNpgsql(Configuration.GetConnectionString(ConnectionStrings.MyApiDbContext),
                    x => x.MigrationsAssembly(typeof(MyApiDbContext).Assembly.FullName)));

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc(WebDefaults.ApiDocName,
                    new OpenApiInfo
                    {
                        Title = WebDefaults.ApiTitle
                    });

                options.AddSecurityDefinition(WebDefaults.AuthSchemeName,
                    new OpenApiSecurityScheme
                    {
                        Description = $"{WebDefaults.AuthHeaderName}: \"Bearer {{token}}\"",
                        In = ParameterLocation.Header,
                        Name = WebDefaults.AuthHeaderName,
                        Type = SecuritySchemeType.ApiKey,
                        Reference = new OpenApiReference
                        {
                            Id = WebDefaults.AuthSchemeName,
                            Type = ReferenceType.SecurityScheme
                        }
                    });
            });

            services.Scan(scan =>
                scan.FromCallingAssembly()
                    .AddClasses()
                    .AsMatchingInterface());

            LogProvider.Configure(VostokLogging.Logger);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                    c.SwaggerEndpoint($"/swagger/{WebDefaults.ApiDocName}/swagger.json", WebDefaults.ApiTitle));
            }

            app.UseAuthentication();
            app.UseRouting();

            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers()
                    .RequireAuthorization();
            });
        }
    }
}