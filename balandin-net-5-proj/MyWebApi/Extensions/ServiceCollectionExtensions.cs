﻿using Microsoft.Extensions.DependencyInjection;
using MyWebApi.DataAccess;
using MyWebApi.DataAccess.Users;

namespace MyWebApi.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddIdentity(this IServiceCollection services)
        {
            services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
                {
                    options.Password.RequiredLength = 5;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireDigit = false;
                })
                .AddEntityFrameworkStores<MyApiDbContext>();

            return services;
        }
    }
}