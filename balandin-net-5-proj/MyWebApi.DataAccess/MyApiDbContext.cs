﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using MyWebApi.DataAccess.Users;

namespace MyWebApi.DataAccess
{
    public class MyApiDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid>
    {
        public MyApiDbContext(DbContextOptions<MyApiDbContext> options, bool useLazyLoad = false) : base(options)
        {
            UseLazyLoad = useLazyLoad;
        }

        public bool UseLazyLoad { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies(UseLazyLoad).ConfigureWarnings(builder =>
                builder.Throw(RelationalEventId.QueryPossibleExceptionWithAggregateOperatorWarning));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(MyApiDbContext).Assembly);
            base.OnModelCreating(modelBuilder);
        }
    }
}