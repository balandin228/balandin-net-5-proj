﻿using System;
using Microsoft.AspNetCore.Identity;

namespace MyWebApi.DataAccess.Users
{
    public class ApplicationUser : IdentityUser<Guid>
    {
    }
}